package com.codeartist.demo.service.impl;

import com.codeartist.demo.service.DemoService;

/**
 * @author 码匠
 * @date 2022/3/10
 */
public class DemoServiceImpl implements DemoService {

    @Override
    public void doService(boolean flag) {
        if (flag) {
            System.out.println("Enter flag.");
        } else {
            System.out.println("Not enter flag.");
        }
    }
}
