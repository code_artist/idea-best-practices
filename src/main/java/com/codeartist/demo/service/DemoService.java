package com.codeartist.demo.service;

/**
 * @author 码匠
 * @date 2022/3/10
 */
public interface DemoService {

    void doService(boolean flag);
}
