package com.codeartist.demo.test;

import com.codeartist.demo.service.DemoService;
import com.codeartist.demo.service.impl.DemoServiceImpl;
import org.junit.Test;

/**
 * @author 码匠
 * @date 2022/3/10
 */
public class DemoServiceTest {

    @Test
    public void doService() {
        DemoService demoService = new DemoServiceImpl();
        demoService.doService(true);
    }
}